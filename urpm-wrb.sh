#!/usr/bin/env bash
# urpm-wrb: what to rebuild after ABI breakage or major so version bump
# Authors:
#  - Mikhail Novosyolov <m.novosyolov@rosalinux.ru>, 2019

echo_help(){
	echo "
urpm-wrb helps to find names of source packages which have to be rebuilt e.g. after ABI breakage or major so version bump.

Usage:
  urpm-wrb <list of packages>
Examples:
  urpm-wrb lib64krb5_3
  urpm-wrb lib64krb5_3 lib64kdb5_8 lib64kadm5srv_mit10
"
exit "$1"
}

TMP="$(mktemp)"
trap 'rm -rf "$TMP"' EXIT INT QUIT TERM

_main_method2(){
	while read -r line
	do
		while read -r line2
		do
			urpmq --sourcerpm "$line2" | awk -F ': ' '{print $2}' | rev | cut -d '-' -f 3- | rev
		done < <(urpmq --whatrequires "$line")
	done < <(echo "$@" | sed -e "s, ,\\n,g")
}

_main(){
	# First try the fastest method, but it will not work if either of packages from the list
	# does not have reverse dependencies
	urpmq --whatrequires --sourcerpm "$@" | awk -F ': ' '{print $2}' | rev | cut -d '-' -f 3- | rev | sort -u > "$TMP"
	method1_rc="${PIPESTATUS[0]}"
	if [ "$method1_rc" != 1 ]
		then if [ "$(cat "$TMP" | wc -l)" -gt 0 ]; then cat "$TMP"; return; fi
		else exit 1
	fi
	
	# If nothing was found and if there were no errors, try a longer method
	_main_method2 "$@" | sort -u
}

case "$1" in
	-h | --help | -help ) echo_help 0 ;;
	* ) if [ -n "$1" ]; then _main "$@"; else echo_help 2 ; fi ;;
esac
