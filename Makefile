SUBDIRS = rpm5utils
PKGNAME = urpm-tools
PYTHON_UTILS = urpm-downloader urpm-package-cleanup urpm-repodiff urpm-repomanage urpm-repograph urpm-reposync
PERL_UTILS = urpm-repoclosure

PYTHON=python
PYFILES = $(wildcard *.py)
PYVER := $(shell $(PYTHON) -c 'import sys; print "%.3s" %(sys.version)')
PYSYSDIR := $(shell $(PYTHON) -c 'import sys; print sys.prefix')
PYLIBDIR = $(PYSYSDIR)/lib/python$(PYVER)
PKGDIR = $(PYLIBDIR)/site-packages
SHELL=/bin/bash
all:
	@echo "Nothing to do. Run 'make install' or 'make clean'"

clean:
	rm -f *.pyc *.pyo *~
	rm -f test/*~
	rm -f *.tar.gz

install:
	mkdir -p $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/usr/share/man/man1
	for util in $(PYTHON_UTILS); do \
		install -m 755 $$util.py $(DESTDIR)/usr/bin/$$util; \
		install -m 664 docs/$$util.1 $(DESTDIR)/usr/share/man/man1/$$util.1; \
	done

	for util in $(PERL_UTILS); do \
		install -m 755 $$util.pl $(DESTDIR)/usr/bin/$$util; \
		install -m 664 docs/$$util.1 $(DESTDIR)/usr/share/man/man1/$$util.1; \
	done

	for d in $(SUBDIRS); do make DESTDIR=$(DESTDIR) -C $$d install; [ $$? = 0 ] || exit 1; done

	install -m 644 urpmmisc.py $(DESTDIR)/$(PKGDIR)/urpmmisc.py;
	install -m 755 urpm-wrb.sh $(DESTDIR)/usr/bin/urpm-wrb


	for d in `python localizer.py --list`; do\
             mkdir -p $(DESTDIR)/usr/share/locale/$$d/LC_MESSAGES;\
             install -m 644 locale/$$d/LC_MESSAGES/urpm-tools.mo $(DESTDIR)/usr/share/locale/$$d/LC_MESSAGES/urpm-tools.mo;\
        done

