
import rpm5utils.urpmgraphs
from rpm5utils.urpmgraphs import *

class Rpm5UtilsError(Exception):

    """ Exception thrown for anything rpm5utils related. """

    def __init__(self, args=None):
        Exception.__init__(self, args)
