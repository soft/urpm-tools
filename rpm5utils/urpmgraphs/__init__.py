"""
NetworkX
========

    NetworkX (NX) is a Python package for the creation, manipulation, and
    study of the structure, dynamics, and functions of complex networks.

    https://networkx.lanl.gov/

Using
-----

    Just write in Python

    >>> import networkx as nx
    >>> G=nx.Graph()
    >>> G.add_edge(1,2)
    >>> G.add_node("spam")
    >>> print(G.nodes())
    [1, 2, 'spam']
    >>> print(G.edges())
    [(1, 2)]
"""
#    Copyright (C) 2004-2010 by
#    Aric Hagberg <hagberg@lanl.gov>
#    Dan Schult <dschult@colgate.edu>
#    Pieter Swart <swart@lanl.gov>
#    All rights reserved.
#    BSD license.
#
# Add platform dependent shared library path to sys.path
#

from __future__ import absolute_import

import sys
if sys.version_info[:2] < (2, 6):
    m = "Python version 2.6 or later is required for NetworkX (%d.%d detected)."
    raise ImportError(m % sys.version_info[:2])
del sys

# Release data

# these packages work with Python >= 2.6
from rpm5utils.urpmgraphs.exception import  *
import rpm5utils.urpmgraphs.classes
from rpm5utils.urpmgraphs.classes import *
import rpm5utils.urpmgraphs.convert
from rpm5utils.urpmgraphs.convert import *
#import urpmgraphs.relabel
#from urpmgraphs.relabel import *
#import urpmgraphs.generators
#from urpmgraphs.generators import *
#from urpmgraphs.readwrite import *
#import urpmgraphs.readwrite
#Need to test with SciPy, when available
import rpm5utils.urpmgraphs.algorithms
from rpm5utils.urpmgraphs.algorithms import *
#import urpmgraphs.linalg
#from urpmgraphs.linalg import *
#from urpmgraphs.tests.test import run as test
#import urpmgraphs.utils

#import urpmgraphs.drawing
#from urpmgraphs.drawing import *

