'''
" Miscellaneous routines used by urpm-tools
"
" Taken from yum's misc.py
'''

import types

_share_data_store   = {}
_share_data_store_u = {}
def share_data(value):
    """ Take a value and use the same value from the store,
        if the value isn't in the store this one becomes the shared version. """
    #  We don't want to change the types of strings, between str <=> unicode
    # and hash('a') == hash(u'a') ... so use different stores.
    #  In theory eventaully we'll have all of one type, but don't hold breath.
    store = _share_data_store
    if isinstance(value, unicode):
        store = _share_data_store_u
    # hahahah, of course the above means that:
    #   hash(('a', 'b')) == hash((u'a', u'b'))
    # ...which we have in deptuples, so just screw sharing those atm.
    if type(value) == types.TupleType:
        return value
    return store.setdefault(value, value)

def string_to_prco_tuple(prcoString):
    """returns a prco tuple (name, flags, (e, v, r)) for a string"""

    if type(prcoString) == types.TupleType:
        (n, f, v) = prcoString
    else:
        n = prcoString
        f = v = None

        # We love GPG keys as packages, esp. awesome provides like:
        #  gpg(Fedora (13) <fedora@fedoraproject.org>)
        if n[0] != '/' and not n.startswith("gpg("):
            # not a file dep - look at it for being versioned
            prco_split = n.split()
            if len(prco_split) == 3:
                n, f, v = prco_split

    # now we have 'n, f, v' where f and v could be None and None
    if f is not None and f not in constants.LETTERFLAGS:
        if f not in constants.SYMBOLFLAGS:
            try:
                f = flagToString(int(f))
            except (ValueError,TypeError), e:
                raise Errors.MiscError, 'Invalid version flag: %s' % f
        else:
            f = constants.SYMBOLFLAGS[f]

    if type(v) in (types.StringType, types.NoneType, types.UnicodeType):
        (prco_e, prco_v, prco_r) = stringToVersion(v)
    elif type(v) in (types.TupleType, types.ListType):
        (prco_e, prco_v, prco_r) = v

    #now we have (n, f, (e, v, r)) for the thing specified
    return (n, f, (prco_e, prco_v, prco_r))

###########
# Title: Remove duplicates from a sequence
# Submitter: Tim Peters
# From: http://aspn.activestate.com/ASPN/Cookbook/Python/Recipe/52560
def unique(s):
    """Return a list of the elements in s, but without duplicates.

    For example, unique([1,2,3,1,2,3]) is some permutation of [1,2,3],
    unique("abcabc") some permutation of ["a", "b", "c"], and
    unique(([1, 2], [2, 3], [1, 2])) some permutation of
    [[2, 3], [1, 2]].

    For best speed, all sequence elements should be hashable.  Then
    unique() will usually work in linear time.

    If not possible, the sequence elements should enjoy a total
    ordering, and if list(s).sort() doesn't raise TypeError it's
    assumed that they do enjoy a total ordering.  Then unique() will
    usually work in O(N*log2(N)) time.

    If that's not possible either, the sequence elements must support
    equality-testing.  Then unique() will usually work in quadratic
    time.
    """

    n = len(s)
    if n == 0:
        return []

    # Try using a set first, as that's the fastest and will usually
    # work.  If it doesn't work, it will usually fail quickly, so it
    # usually doesn't cost much to *try* it.  It requires that all the
    # sequence elements be hashable, and support equality comparison.
    try:
        u = set(s)
    except TypeError:
        pass
    else:
        return list(u)

    # We can't hash all the elements.  Second fastest is to sort,
    # which brings the equal elements together; then duplicates are
    # easy to weed out in a single pass.
    # NOTE:  Python's list.sort() was designed to be efficient in the
    # presence of many duplicate elements.  This isn't true of all
    # sort functions in all languages or libraries, so this approach
    # is more effective in Python than it may be elsewhere.
    try:
        t = list(s)
        t.sort()
    except TypeError:
        del t  # move on to the next method
    else:
        assert n > 0
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]

    # Brute force is all that's left.
    u = []
    for x in s:
        if x not in u:
            u.append(x)
    return u

def GetUrlFromRepoName(reponame):
    urpmi = open("/etc/urpmi/urpmi.cfg")
    if not urpmi:
        print "cannot open file urpmi.cfg"
        return None
    i = 0
    repo_dict = {}
    name = ''
    isignore = 0
    isupdate = 0
    mirrorlist = ''
    withdir = ''
    for line in urpmi:
        line = line.strip()
        if line.endswith('{'):
            line = line[:-1].strip()
            line = line.lower()
            line = line.split("\ ")
            line = ' '.join(line)
            name = line
        elif line.startswith("ignore"):
            isignore = 1
        elif line.startswith("update"):
            isupdate = 1
        elif line.startswith("mirrorlist: "):
            line = line[12:]
            if not line.startswith('$'):
                if not line.endswith('/'):
                    line = line + '/'
                mirrorlist = line
        elif line.startswith("with-dir: "):
            line = line[10:]
            withdir = line
        elif line.startswith('}'):
            if mirrorlist == '':
                path = None
            else:
                path = mirrorlist + withdir
            if (name) and (path):
                repo_dict[name]=(isignore, isupdate, path)
            name = ''
            isignore = 0
            isupdate = 0
            mirrorlist = ''
            withdir = ''
    urpmi.close()
    name2 = reponame.lower()
    if name2 not in repo_dict:
        return (None, None, None)
    else:
        return repo_dict[name2]
