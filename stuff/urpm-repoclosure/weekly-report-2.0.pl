#!/usr/bin/perl
# Send mail about repoclosure results to ROSA-Devel

use File::Path qw(mkpath rmtree);
use strict;

my $ADDR = "rosa-devel\@lists.rosalab.ru";
# my $ADDR = "aponomarenko\@rosalab.ru";

my ($Count_Cur, $Pkg_Dep_Cur);
my ($Count_Old, $Pkg_Dep_Old);

sub writeFile($$)
{
    my ($Path, $Content) = @_;
    return if(not $Path);
    open (FILE, ">".$Path) || die "can't open file \'$Path\': $!\n";
    print FILE $Content;
    close(FILE);
}

sub readFile($)
{
    my $Path = $_[0];
    open (FILE, $Path);
    local $/ = undef;
    my $Content = <FILE>;
    close(FILE);
    return $Content;
}

sub showTable($)
{
    my $R = $_[0];
    
    my $Table = "";
    
    if($Count_Cur->{$R})
    {
        $Table .= "\n[".uc($R)."]\n";
        
        foreach my $P (sort {lc($a) cmp lc($b)} keys(%{$Pkg_Dep_Cur->{$R}}))
        {
            foreach my $D (sort {lc($a) cmp lc($b)} keys(%{$Pkg_Dep_Cur->{$R}{$P}}))
            {
                $Table .= $P.": \"".$D."\"\n";
            }
        }
    }
    
    return $Table;
}

sub readReports($)
{
    my $Dir = $_[0];
    
    if(not -d $Dir) {
        return ();
    }
    
    my (%Count, %Pkg_Dep) = @_;
    
    foreach my $R ("main", "contrib", "non-free", "restricted")
    {
        my $Content = readFile($Dir."/i586/$R/report.txt");
    
        if($Content=~s/Broken Dependency\s\((\d+)\)//i) {
            $Count{$R} = $1;
        }
        
        while($Content=~s/(.+) \(required by ([^()]+)//) {
            $Pkg_Dep{$R}{$2}{$1} = 1;
        }
    }
    
    return (\%Count, \%Pkg_Dep);
}

sub scenario()
{
    my $REPORTS_CUR = "rosa2014.1";
    
    mkpath("prev");
    
    ($Count_Cur, $Pkg_Dep_Cur) = readReports($REPORTS_CUR);
    ($Count_Old, $Pkg_Dep_Old) = readReports("prev/".$REPORTS_CUR);
    
    my (%Fixed, %New) = ();
    my ($Fixed_C, $New_C) = (0, 0);
    
    foreach my $R ("main", "contrib", "non-free", "restricted")
    {
        foreach my $P (keys(%{$Pkg_Dep_Old->{$R}}))
        {
            foreach my $D (keys(%{$Pkg_Dep_Old->{$R}{$P}}))
            {
                if(not defined $Pkg_Dep_Cur->{$R}{$P}{$D})
                {
                    $Fixed{$R}{$P}{$D} = 1;
                    $Fixed_C+=1;
                }
            }
        }
        foreach my $P (keys(%{$Pkg_Dep_Cur->{$R}}))
        {
            foreach my $D (keys(%{$Pkg_Dep_Cur->{$R}{$P}}))
            {
                if(not defined $Pkg_Dep_Old->{$R}{$P}{$D})
                {
                    $New{$R}{$P}{$D} = 1;
                    $New_C+=1;
                }
            }
        }
    }
    
    my $Broken = $Count_Cur->{"main"}+$Count_Cur->{"non-free"}+$Count_Cur->{"restricted"}+$Count_Cur->{"contrib"};
    
    my $MAIL = "Subject: [FBA] Weekly repoclosure status of ROSA Fresh: $Broken broken ";
    
    if($Broken==1) {
        $MAIL .= "package";
    }
    else {
        $MAIL .= "packages";
    }
    
    $MAIL .= "\nTo: ROSA Developers <$ADDR>";
    
    if($Broken==1) {
        $MAIL .= "\nThere is $Broken broken package";
    }
    else {
        $MAIL .= "\nThere are $Broken broken packages";
    }
    $MAIL .= " in the repositories of ROSA Fresh on i586.\n";
    $MAIL .= "\nPlease see detailed report here: http://upstream-tracker.org/repoclosure_reports/\n";
    
    $MAIL .= "\nFixed this week";
    if($Fixed_C)
    {
        $MAIL .= " (".$Fixed_C."):\n";
        foreach my $R ("main", "contrib", "non-free", "restricted")
        {
            foreach my $P (sort {lc($a) cmp lc($b)} keys(%{$Fixed{$R}}))
            {
                foreach my $D (sort {lc($a) cmp lc($b)} keys(%{$Fixed{$R}{$P}}))
                {
                    $MAIL .= "$P: \"$D\" [".uc($R)."]\n";
                }
            }
        }
    }
    else {
        $MAIL .= ": 0\n";
    }
    
    $MAIL .= "\nNew this week";
    if($New_C)
    {
        $MAIL .= " (".$New_C."):\n";
        foreach my $R ("main", "contrib", "non-free", "restricted")
        {
            foreach my $P (sort {lc($a) cmp lc($b)} keys(%{$New{$R}}))
            {
                foreach my $D (sort {lc($a) cmp lc($b)} keys(%{$New{$R}{$P}}))
                {
                    $MAIL .= "$P: \"$D\" [".uc($R)."]\n";
                }
            }
        }
    }
    else {
        $MAIL .= ": 0\n";
    }
    
    $MAIL .= "\nHere is the list of all broken packages in the format \"package: broken dependency\":\n";
    
    $MAIL .= showTable("main");
    $MAIL .= showTable("non-free");
    $MAIL .= showTable("restricted");
    $MAIL .= showTable("contrib");
    
    $MAIL .= "\n";
    
    writeFile("mail", $MAIL);
    
    system("/usr/sbin/sendmail $ADDR < mail");

    unlink("mail");
    
    # save old reports
    system("cp -fr $REPORTS_CUR prev/");
}

scenario();
