#!/usr/bin/python2.7
# -*- coding: UTF-8 -*-

import os, sys

quiet = False
if '--list' in sys.argv:
  quiet = True
  
def qprint(text):
  if quiet:
    sys.stderr.write(text + '\n')
    sys.stderr.flush()
    return
  print text
  
def dumb(cmd):
  if quiet:
    return cmd + ' 1>&2'
  else:
    return cmd

walkres = os.walk('.')
fls = []
pos = []

for path, dirs, files in walkres:
  for file in files:
    p = os.path.join(path, file)
    if p.endswith(".py"):
      fls.append(p)
    if p.endswith(".pl"):
      fls.append(p)
    if p.endswith(".po"):
      pos.append(p)
    
if not fls:
  qprint("No python modules found!")
  exit(1)
  
  
FN = 'urpm-tools.pot'

qprint("Generating " + FN)
cmd = "xgettext -d urpm-tools -o " + FN + ' -c --no-wrap ' + ' '.join(fls)
os.system(dumb(cmd))
  
LIST_OUT = []
for po in pos:
  qprint("Updating " + po)
  LIST_OUT.append(po.split('/')[2])
  
  cmd = "msgmerge --no-wrap -U  " + po + ' ' + FN
  os.system(dumb(cmd))
  mo = po[:-2] + 'mo'
  qprint ("Compiling " + po)
  cmd = "msgfmt -o " + mo + ' ' + po
  os.system(dumb(cmd))

if quiet:
  print ' '.join(LIST_OUT)
  